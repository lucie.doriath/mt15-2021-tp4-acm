numpy == 1.20.1
pandas == 1.2.2
matplotlib == 3.3.4
xlrd == 2.0.1
scikit-learn == 0.24.1
fanalysis == 0.0.1
